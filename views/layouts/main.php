<!DOCTYPE html>
<html lang="ru">
<head>
    <title>NixQ</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width" />
    <link rel="shortcut icon" href="/design/images/favicon.ico" type="image/x-icon" />
    <!-- Bootstrap Css -->
    <link rel="stylesheet" href="/design/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/design/bootstrap/css/bootstrap-theme.min.css"/>
    <!-- Font-awesome Css -->
    <link rel="stylesheet" href="/design/css/font-awesome.min.css"/>
    <!-- Custom Radio&Checkboxes Css -->
    <link rel="stylesheet" href="/design/css/check.css"/>
    <!-- Select2 v4.0.0 Css -->
    <link rel="stylesheet" href="/design/css/select2.min.css"/>
    <!-- Select2 v4.0.0 Css -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,600,700&subset=latin,cyrillic' rel='stylesheet'
          type='text/css'>
    <!-- Custom Css -->
    <link rel="stylesheet" href="/design/css/style.css"/>

    <link rel="stylesheet" href="/design/main.css"/>
    <!-- jQuery v1.11.2 -->
    <script src="/design/js/jquery-1.11.2.min.js"></script>
    <!-- Placeholder IE9-->
    <script src="/design/js/jquery.placeholder.min.js"></script>
    <!-- Select2 v4.0.0 -->
    <script src="/design/js/select2.min.js"></script>
    <!-- iCheck -->
    <script src="/design/js/icheck.min.js"></script>
    <!-- Custom Js -->
    <script src="/design/js/main.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="/design/js/html5shiv.js"></script>
    <script src="/design/js/respond.js"></script>
    <![endif]-->
</head>
<body>
<div id="main">
    <header id="header">
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <h1 class="logo">
                        <a href="/" title="NixQ">
                            <img class="img-responsive" src="/design/images/site/logo.png" alt="NixQ"
                                 title="NixQ. Найди ответ на свой вопрос."/>
                        </a>
                    </h1>
                </div>
                <nav class="col-md-10">
                    <ul class="log-in">
                        <li><a href="#" title="Логин"><i class="fa fa-user" aria-hidden="true"></i></a></li>
                        <li><a href="#" title="Регистрация"><i class="fa fa-sign-in" aria-hidden="true"></i></a></li>
                    </ul>
                    <ul class="main-menu">
                        <li><a href="/question/popular" title="Популярное">Популярное</a></li>
                        <li><a href="/category" title="Категории">Категории</a></li>
                    </ul>
                    <div class="btn-base-container">
                        <a href="/question/add" title="Задать вопрос" class="btn-base">Задать вопрос</a>
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <div id="main-content">
            <?= $content ?>
        <a href="#" title="Back To Top" id="back-to-top"><i class="fa fa-chevron-up"></i></a>
    </div>
    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="copyright col-md-6">
                    <p>&copy; 2016 NixQ. NixSolutions</p>
                </div>
                <div class="col-md-6 nix-logo">
                    <a href="http://www.nixsolutions.com/" title="NixSolutions">
                        <img class="img-responsive" src="/design/images/site/logo-nix.png" alt="NixSolutions"
                             title="NixSolutions. Software Development Company."/>
                    </a>
                </div>
            </div>
        </div>
    </footer>
</div>
</body>
</html>