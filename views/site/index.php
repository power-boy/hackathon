
    <div class="banner">
        <img src="/design/images/temp/banner.png" class="img-responsive" alt="NixQ" title="NixQ. Найди ответ на свой вопрос." />
        <div class="banner-wrapper">
            <div class="container">
                <div class="banner-description">
                    <h2 class="banner-title"><span>Есть вопрос?</span>Найди ответ на него!</h2>
                    <div class="search-form">
                        <div class="search-input">
                            <form action="/question/search">
                                <input type="search" name="title" placeholder="Поиск...">
                                <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="about main-section blue-content">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title"><span>Как это работает?</span></h2>
            </div>
            <div class="section-content">
                <ul class="about-list">
                    <li class="col-md-4">
                        <div class="item-icon">
                            <i class="fa fa-question" aria-hidden="true"></i>
                        </div>
                        <div class="description">
                            <h4 class="step-title"><strong>Что-то ищешь или есть вопрос?</strong></h4>
                            <p class="step-description">
                                Ищешь проверенного специалиста или уже нашел и хочешь поделиться? Не знаешь у кого спросить как оформить визу или сам можешь поделится опытом? А возможно хочешь одолжить вещь за вкусняшку? Тогда ты по адресу!
                            </p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="item-icon">
                            <i class="fa fa-list-alt" aria-hidden="true"></i>
                        </div>
                        <div class="description">
                            <h4 class="step-title"><strong>Наши советы помогут тебе!</strong></h4>
                            <p class="step-description">Этот сервис создан специально для обмена советами и рекомендациями. Ты в считанные минуты можешь найти нужного тебе специалиста, одолжить интересующую тебя вешь. Или разместить свое объявление про котиков! И все это через простой и удобный интерфейс!</p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="item-icon">
                            <i class="fa fa-star" aria-hidden="true"></i>
                        </div>
                        <div class="description">
                            <h4 class="step-title"><strong>Радуйся и делись сервисом!</strong></h4>
                            <p class="step-description">Нашел решение своей проблемы у нас? Поздравляем! Обязательно поделись своим опытом с колегами, и тогда у каждого из нас будет качественный ремонт и здоровые зубы! Ведь наш полезный опыт может принести пользу не только нам!</p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="ask main-section white-content purple">
        <div class="container">
            <div class="ask-wrapper">
                <h2 class="section-title">Задай свой вопрос</h2>
                <div class="btn-base-container">
                    <a href="/question/add" title="Задать вопрос" class="btn-base">Задать вопрос</a>
                </div>
            </div>
        </div>
    </div>
    <div class="popular main-section">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title"><span>Популярные</span></h2>
            </div>
            <div class="section-content">
                <ul class="question-list">
                    <?php foreach ($questions as $question) { ?>
                        <li class="col-md-4 question <?=$question->style_title?>">
                            <div class="question-header">
                                <span class="question-action"><i class="fa fa-comments" aria-hidden="true"></i><span class="question-action-number">37</span></span>
                                <span class="question-action"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span class="question-action-number">125</span></span>
                                <span class="question-action"><i class="fa fa-thumbs-down" aria-hidden="true"></i><span class="question-action-number">14</span></span>
                                <a href="/category/view?id=<?=$question->category_id?>" title="Отдых" class="question-category"> <i class="fa fa-sun-o" aria-hidden="true"></i></a>
                            </div>
                            <div class="question-content">
                                <h3 class="question-title"><a href="/question/view?id=<?=$question->id?>" title="Lorem ipsum dolor"><?=$question->title?></a></h3>
                                <div class="question-description">
                                    <p><?=$question->description?></p>
                                </div>
                                <div class="question-posted row">
                                    <div class="question-posted-info col-md-6">
                                        <p class="question-author"><a href="#" title="Вася Пупкин">Вася Пупкин</a></p>
                                        <p class="question-date">15.04.2016</p>
                                    </div>
                                    <div class="question-rating col-md-6">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
                <div class="btn-base-container">
                    <a href="/question/popular" title="Другие вопросы" class="btn-base">Другие вопросы</a>
                </div>
            </div>
        </div>
    </div>
    <div class="categories main-section white-content blue">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title"><span>Поиск по категориям</span></h2>
            </div>
            <div class="section-content">
                <ul class="category-list">
                    <li class="col-md-2">
                        <a href="/category/view?id=1" title="Недвижимость" class="category house">
                            <div class="item-icon">
                                <i class="fa fa-home" aria-hidden="true"></i>
                            </div>
                            <h3 class="item-title">Недвижимость</h3>
                        </a>
                    </li>
                    <li class="col-md-2">
                        <a href="/category/view?id=2"  class="category cars">
                            <div class="item-icon">
                                <i class="fa fa-car" aria-hidden="true"></i>
                            </div>
                            <h3 class="item-title">Авто</h3>
                        </a>
                    </li>
                    <li class="col-md-2">
                        <a href="/category/view?id=3" title="Отдых" class="category recreation">
                            <div class="item-icon">
                                <i class="fa fa-sun-o" aria-hidden="true"></i>
                            </div>
                            <h3 class="item-title">Отдых</h3>
                        </a>
                    </li>
                    <li class="col-md-2">
                        <a href="/category/view?id=4" title="Животные" class="category pets">
                            <div class="item-icon">
                                <i class="fa fa-paw" aria-hidden="true"></i>
                            </div>
                            <h3 class="item-title">Животные</h3>
                        </a>
                    </li>
                    <li class="col-md-2">
                        <a href="/category/view?id=5" title="Спорт" class="category sport">
                            <div class="item-icon">
                                <i class="fa fa-futbol-o" aria-hidden="true"></i>
                            </div>
                            <h3 class="item-title">Спорт</h3>
                        </a>
                    </li>
                    <li class="col-md-2">
                        <a href="/category/view?id=6"  class="category medicine">
                            <div class="item-icon">
                                <i class="fa fa-heartbeat" aria-hidden="true"></i>
                            </div>
                            <h3 class="item-title">Медицина</h3>
                        </a>
                    </li>
                </ul>
                <div class="btn-base-container">
                    <a href="/category" title="Все категории" class="btn-base">Все категории</a>
                </div>
            </div>
        </div>
    </div>
