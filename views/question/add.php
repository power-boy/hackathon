
    <div class="container">
        <div class="page-title row">
            <h2 class="col-md-12"><span>Создать вопрос</span></h2>
        </div>
        <div class="page-content row">
            <form method="post">
                <div class="form-row">
                    <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                    <div class="col-md-6">
                        <div class="form-col">
                            <input type="text" name="title" placeholder="Введите название вопроса" />
                        </div>
                        <div class="form-col">
                            <select name="category_id">
                                <option value="1">Недвижимость</option>
                                <option value="2">Авто</option>
                                <option value="3">Отдых</option>
                                <option value="4">Животные</option>
                                <option value="5">Спорт</option>
                                <option value="6">Медицина</option>
                            </select>
                        </div>
                        <div class="form-col">
                            <textarea name="description" placeholder="Описание вопроса"></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-col">
                            <div class="map">
                                <img class="img-responsive" src="/design/images/temp/map.png" alt="Карта" title="Карта." />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-action col-md-12">
                    <div class="btn-base-container">
                        <button type="submit" class="btn-base confirm"><i class="fa fa-check" aria-hidden="true"></i>Готово</button>
                        <button formaction="/" class="btn-base reject"><i class="fa fa-times" aria-hidden="true"></i>Отмена</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <a href="#" title="Back To Top" id="back-to-top"><i class="fa fa-chevron-up"></i></a>
