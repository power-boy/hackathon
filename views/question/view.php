<div class="container">
    <div class="page-title row">
        <h2 class="col-md-12"><span>Вопрос</span></h2>
    </div>
    <div class="row">
        <div class="page-content">
            <div class="col-md-9">
                <div class="question <?=$question->style_title?>">
                    <div class="question-header">
                        <span class="question-action"><i class="fa fa-comments" aria-hidden="true"></i><span class="question-action-number">37</span></span>
                        <span class="question-action"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span class="question-action-number">125</span></span>
                        <span class="question-action"><i class="fa fa-thumbs-down" aria-hidden="true"></i><span class="question-action-number">14</span></span>
                        <a href="/category/view/id=<?=$question->category_id?>"  class="question-category"> <i class="<?=$question->icon?>" aria-hidden="true"></i></a>
                    </div>
                    <div class="question-content">
                        <h3 class="question-title"><a href="#" title="<?=$question->title?>"><?=$question->title?></a></h3>
                        <div class="question-description">
                            <p><?=$question->description?></p>
                        </div>
                        <div class="question-posted row">
                            <div class="question-posted-info col-md-6">
                                <p class="question-author"><a href="#" title="Вася Пупкин">Вася Пупкин</a></p>
                                <p class="question-date">15.04.2016</p>
                            </div>
                            <div class="question-rating col-md-6">
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                <i class="fa fa-star-o" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>

                    <div class="comments">
                        <aside>
                            <h4 class="aside-title">Комментарии</h4>

                            <form>
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <div class="form-col">
                                            <textarea rows="3" placeholder="Ваш комментарий..."></textarea>
                                        </div>
                                    </div>
                                    <div class="form-action col-md-12">
                                        <div class="btn-base-container">
                                            <button onclick="return false;" class="btn-base confirm"><i class="fa fa-check"
                                                                                aria-hidden="true"></i>Отправить
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </aside>
                    </div>
                </div>
            </div>
            <div class="col-md-3 sidebar right-sidebar">
                <aside>
                    <div class="map">
                        <img class="img-responsive" src="/design/images/temp/map.png" alt="Карта" title="Карта." />
                    </div>
                </aside>
                <aside>
                    <ul class="question-list same-question">
                        <li class="question <?=$question->style_title?>">
                            <div class="question-header">
                                <span class="question-action"><i class="fa fa-comments" aria-hidden="true"></i><span class="question-action-number">37</span></span>
                                <span class="question-action"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span class="question-action-number">125</span></span>
                                <span class="question-action"><i class="fa fa-thumbs-down" aria-hidden="true"></i><span class="question-action-number">14</span></span>
                                <a href="/category/view?id=<?=$question->category_id?>"  class="question-category"> <i class="<?=$question->icon?>" aria-hidden="true"></i></a>
                            </div>
                            <div class="question-content">
                                <h3 class="question-title"><a href="#" title="Lorem ipsum dolor">Lorem ipsum dolor sit amet, ex reque lobortis eam, ad meis primis</a></h3>

                                <div class="question-posted row">
                                    <div class="question-posted-info col-md-6">
                                        <p class="question-author"><a href="#" title="Вася Пупкин">Вася Пупкин</a></p>
                                        <p class="question-date">15.04.2016</p>
                                    </div>
                                    <div class="question-rating col-md-6">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="question <?=$question->style_title?>">
                            <div class="question-header">
                                <span class="question-action"><i class="fa fa-comments" aria-hidden="true"></i><span class="question-action-number">37</span></span>
                                <span class="question-action"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span class="question-action-number">125</span></span>
                                <span class="question-action"><i class="fa fa-thumbs-down" aria-hidden="true"></i><span class="question-action-number">14</span></span>
                                <a href="/category/view?id=<?=$question->category_id?>"  class="question-category"> <i class="<?=$question->icon?>" aria-hidden="true"></i></a>
                            </div>
                            <div class="question-content">
                                <h3 class="question-title"><a href="#" title="Lorem ipsum dolor">Lorem ipsum dolor sit amet, ex reque lobortis eam, ad meis primis</a></h3>

                                <div class="question-posted row">
                                    <div class="question-posted-info col-md-6">
                                        <p class="question-author"><a href="#" title="Вася Пупкин">Вася Пупкин</a></p>
                                        <p class="question-date">15.04.2016</p>
                                    </div>
                                    <div class="question-rating col-md-6">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="question <?=$question->style_title?>">
                            <div class="question-header">
                                <span class="question-action"><i class="fa fa-comments" aria-hidden="true"></i><span class="question-action-number">37</span></span>
                                <span class="question-action"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span class="question-action-number">125</span></span>
                                <span class="question-action"><i class="fa fa-thumbs-down" aria-hidden="true"></i><span class="question-action-number">14</span></span>
                                <a href="/category/view?id=<?=$question->category_id?>"  class="question-category"> <i class="<?=$question->icon?>" aria-hidden="true"></i></a>
                            </div>
                            <div class="question-content">
                                <h3 class="question-title"><a href="#" title="Lorem ipsum dolor">Lorem ipsum dolor sit amet, ex reque lobortis eam, ad meis primis</a></h3>

                                <div class="question-posted row">
                                    <div class="question-posted-info col-md-6">
                                        <p class="question-author"><a href="#" title="Вася Пупкин">Вася Пупкин</a></p>
                                        <p class="question-date">15.04.2016</p>
                                    </div>
                                    <div class="question-rating col-md-6">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </aside>
            </div>
        </div>
    </div>
</div>