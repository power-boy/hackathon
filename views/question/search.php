<div class="container categories-page">
<div class="page-title row">
    <h2 class="col-md-6"><span>Результаты по слову "<?=$title?>"</span></h2>

    <div class="search-form col-md-6">
        <div class="search-form">
            <div>
                <form class="search-input" action="/question/search">
                    <input type="search" name="title" placeholder="Поиск...">
                    <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                </form>

            </div>
        </div>
    </div>
</div>
<div class="row">
<div class="page-content">
<div class="col-md-3 sidebar left-sidebar">
    <aside>
        <h3 class="">Фильтр</h3>
    </aside>
    <aside>
        <h3 class="aside-title">По карегории</h3>

        <form>
            <div class="form-col">
                <select>

                    <option>Все категории</option>
                    <option>Недвижимость</option>
                    <option>Авто</option>
                    <option>Отдых</option>
                    <option>Животные</option>
                    <option>Спорт</option>
                    <option>Медицина</option>
                </select>
            </div>

        </form>


    </aside>

    <aside>
        <h4 class="aside-title">По типу</h4>
        <form>
                <div class="form-col">
                    <div class="radio-elem">
                        <input type="radio" id="type-ad" name="type" checked>
                        <label for="type-ad">Объявления</label>
                    </div>
                    <div class="radio-elem">
                        <input type="radio" id="type-question" name="type">
                        <label for="type-question">Вопросы</label>
                    </div>
                    <div class="radio-elem">
                        <input type="radio" id="type-specialist" name="type">
                        <label for="type-specialist">Специалисты</label>
                    </div>
                    <div class="radio-elem">
                        <input type="radio" id="type-thing" name="type">
                        <label for="type-thing">Вещи</label>
                    </div>
                </div>


        </form>
    </aside>

    <aside>
        <h3 class="aside-title">По достоверности</h3>
        <form>
            <div class="form-col">
                <div class="check-elem">
                    <input type="checkbox" id="agree" checked>
                    <label for="agree">Только проверенные</label>
                </div>
            </div>
            <div class="form-action">
                <div class="btn-base-container">
                    <button class="btn-base confirm"><i class="fa fa-check" aria-hidden="true"></i>Применить</button>
                </div>
            </div>
        </form>
    </aside>


</div>
<div class="col-md-9">
<ul class="question-list">
    <?php foreach($questions as $question) :?>
<li class=" question <?=$question['style_title']?>">
    <div class="question-header">
        <span class="question-action"><i class="fa fa-comments" aria-hidden="true"></i><span
                class="question-action-number">37</span></span>
        <span class="question-action"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span
                class="question-action-number">125</span></span>
        <span class="question-action"><i class="fa fa-thumbs-down" aria-hidden="true"></i><span
                class="question-action-number">14</span></span>
        <a href="/category/view?id=<?=$question['category_id']?>"  class="question-category"> <i class="<?=$question['icon']?>"
                                                                             aria-hidden="true"></i></a>
    </div>
    <div class="question-content">
        <h3 class="question-title"><a href="/question/view?id=<?=$question['id']?>" title="Lorem ipsum dolor"><?=$question['title']?></a></h3>

        <div class="question-description">
            <p><?=$question['description']?></p>
        </div>
        <div class="question-posted row">
            <div class="question-posted-info col-md-6">
                <p class="question-author"><a href="#" title="Вася Пупкин">Вася Пупкин</a></p>

                <p class="question-date">15.04.2016</p>
            </div>
            <div class="question-rating col-md-6">
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star-half-o" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
            </div>
        </div>
    </div>
</li>
    <?php endforeach; ?>
</ul>
    <?php if ($questions) :?>
<div class="pagination">
    <a href="#" title="First"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a>
    <a href="#" title="Previous"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
    <a href="#" class="current-menu-item" title="1">1</a>
    <a href="#" title="2">2</a>
    <a href="#" title="3">3</a>
    <a href="#" title="...">...</a>
    <a href="#" title="39">39</a>
    <a href="#" title="Next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
    <a href="#" title="Last"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a>

</div>
    <?php endif;?>
</div>

</div>
</div>
</div>