<?php
use \yii\helpers\Url;
?>

<div class="container">
        <div class="categories-page">
        <div class="page-title row">
            <h2 class="col-md-6"><span>Категории</span></h2>
            <div class="search-form col-md-6">
                <div>
                    <form class="search-input" action="/question/search">
                        <input type="search" name="title" placeholder="Поиск...">
                        <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </form>
                </div>
            </div>
        </div>
        <div class="page-content row">
            <ul class="category-list">
                    <li class="col-md-2">
                        <a href="<?= Url::to(['/category/view','id' => 1]) ?>" title="Недвижимость" class="category house">
                            <div class="item-icon">
                                <i class="fa fa-home" aria-hidden="true"></i>
                            </div>
                            <h3 class="item-title">Недвижимость</h3>
                        </a>
                    </li>
                    <li class="col-md-2">
                        <a href="<?= Url::to(['/category/view','id' => 2]) ?>"  class="category cars">
                            <div class="item-icon">
                                <i class="fa fa-car" aria-hidden="true"></i>
                            </div>
                            <h3 class="item-title">Авто</h3>
                        </a>
                    </li>
                    <li class="col-md-2">
                        <a href="<?= Url::to(['/category/view','id' => 3]) ?>" title="Отдых" class="category recreation">
                            <div class="item-icon">
                                <i class="fa fa-sun-o" aria-hidden="true"></i>
                            </div>
                            <h3 class="item-title">Отдых</h3>
                        </a>
                    </li>
                    <li class="col-md-2">
                        <a href="<?= Url::to(['/category/view','id' => 4]) ?>" title="Животные" class="category pets">
                            <div class="item-icon">
                                <i class="fa fa-paw" aria-hidden="true"></i>
                            </div>
                            <h3 class="item-title">Животные</h3>
                        </a>
                    </li>
                    <li class="col-md-2">
                        <a href="<?= Url::to(['/category/view','id' => 5]) ?>" title="Спорт" class="category sport">
                            <div class="item-icon">
                                <i class="fa fa-futbol-o" aria-hidden="true"></i>
                            </div>
                            <h3 class="item-title">Спорт</h3>
                        </a>
                    </li>
                    <li class="col-md-2">
                        <a href="<?= Url::to(['/category/view','id' => 6]) ?>"  class="category medicine">
                            <div class="item-icon">
                                <i class="fa fa-heartbeat" aria-hidden="true"></i>
                            </div>
                            <h3 class="item-title">Медицина</h3>
                        </a>
                    </li>
            </ul>
        </div>
        </div>
</div>
