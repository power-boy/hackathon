<div class="container">
    <div class="page-title row">
        <h2 class="col-md-12"><span>Категория: Авто</span></h2>
    </div>
    <div class="row">
        <div class="page-content">
            <div class="col-md-9">
                <div class="question cars">
                    <div class="question-header">
                        <span class="question-action"><i class="fa fa-comments" aria-hidden="true"></i><span class="question-action-number">37</span></span>
                        <span class="question-action"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span class="question-action-number">125</span></span>
                        <span class="question-action"><i class="fa fa-thumbs-down" aria-hidden="true"></i><span class="question-action-number">14</span></span>
                        <a href="/category/view?id=2/view?id=2"  class="question-category"> <i class="fa fa-car" aria-hidden="true"></i></a>
                    </div>
                    <div class="question-content">
                        <div class="col-md-4">
                            <div class="specialist-view">
                                <div class="category">
                                    <img class="item-icon"
                                         src="http://www.lovemarks.com/wp-content/uploads/profile-avatars/default-avatar-plaid-shirt-guy.png"
                                         alt=""/>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <h1>Вася Автомеханик</h1>
                            <h4>Lorem ipsum dolor sit amet, ex reque lobortis eam, ad meis primis adipiscing his.</h4>
                            <h5><strong>Контакты</strong></h5>
                            <h5>Телефон: +38-050-505-05-05</h5>
                            <h5>Адрес: ул. Пушкинская 4/а</h5>
                        </div>

                        <div class="col-md-3">
                            <div class="question-rating">
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                <i class="fa fa-star-o" aria-hidden="true"></i>
                            </div>
                            <div class="question-posted row text-right">
                                <div class="col-md-6"></div>
                                <div class="question-posted-info col-md-6">
                                    <p class="question-author text">Опубликовал:
                                        <a href="#" title="Вася Пупкин">Вася Пупкин</a>
                                    </p>

                                    <p class="question-date">15.04.2016</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="comments col-md-12">
                        <aside>
                            <h4 class="aside-title">Комментарии</h4>

                            <form>
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <div class="form-col">
                                            <textarea rows="3" placeholder="Ваш комментарий..."></textarea>
                                        </div>
                                    </div>
                                    <div class="form-action col-md-12">
                                        <div class="btn-base-container">
                                            <button class="btn-base confirm"><i class="fa fa-check"
                                                                                aria-hidden="true"></i>Отправить
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </aside>
                    </div>
                </div>
            </div>
            <div class="col-md-3 sidebar right-sidebar">
                <aside>
                    <div class="map">
                        <img class="img-responsive" src="/design/images/temp/map.png" alt="Карта" title="Карта." />
                    </div>
                </aside>
                <aside>
                    <ul class="question-list same-question">
                        <li class="question cars">
                            <div class="question-header">
                                <span class="question-action"><i class="fa fa-comments" aria-hidden="true"></i><span class="question-action-number">37</span></span>
                                <span class="question-action"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span class="question-action-number">125</span></span>
                                <span class="question-action"><i class="fa fa-thumbs-down" aria-hidden="true"></i><span class="question-action-number">14</span></span>
                                <a href="/category/view?id=2"  class="question-category"> <i class="fa fa-car" aria-hidden="true"></i></a>
                            </div>
                            <div class="question-content">
                                <h3 class="question-title"><a href="/category/view?id=2" title="Lorem ipsum dolor">Lorem ipsum dolor sit amet, ex reque lobortis eam, ad meis primis</a></h3>

                                <div class="question-posted row">
                                    <div class="question-posted-info col-md-6">
                                        <p class="question-author"><a href="#" title="Вася Пупкин">Вася Пупкин</a></p>
                                        <p class="question-date">15.04.2016</p>
                                    </div>
                                    <div class="question-rating col-md-6">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="question cars">
                            <div class="question-header">
                                <span class="question-action"><i class="fa fa-comments" aria-hidden="true"></i><span class="question-action-number">37</span></span>
                                <span class="question-action"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span class="question-action-number">125</span></span>
                                <span class="question-action"><i class="fa fa-thumbs-down" aria-hidden="true"></i><span class="question-action-number">14</span></span>
                                <a href="/category/view?id=2"  class="question-category"> <i class="fa fa-car" aria-hidden="true"></i></a>
                            </div>
                            <div class="question-content">
                                <h3 class="question-title"><a href="/category/view?id=2" title="Lorem ipsum dolor">Lorem ipsum dolor sit amet, ex reque lobortis eam, ad meis primis</a></h3>

                                <div class="question-posted row">
                                    <div class="question-posted-info col-md-6">
                                        <p class="question-author"><a href="#" title="Вася Пупкин">Вася Пупкин</a></p>
                                        <p class="question-date">15.04.2016</p>
                                    </div>
                                    <div class="question-rating col-md-6">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="question cars">
                            <div class="question-header">
                                <span class="question-action"><i class="fa fa-comments" aria-hidden="true"></i><span class="question-action-number">37</span></span>
                                <span class="question-action"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span class="question-action-number">125</span></span>
                                <span class="question-action"><i class="fa fa-thumbs-down" aria-hidden="true"></i><span class="question-action-number">14</span></span>
                                <a href="/category/view?id=2"  class="question-category"> <i class="fa fa-car" aria-hidden="true"></i></a>
                            </div>
                            <div class="question-content">
                                <h3 class="question-title"><a href="/category/view?id=2" title="Lorem ipsum dolor">Lorem ipsum dolor sit amet, ex reque lobortis eam, ad meis primis</a></h3>

                                <div class="question-posted row">
                                    <div class="question-posted-info col-md-6">
                                        <p class="question-author"><a href="#" title="Вася Пупкин">Вася Пупкин</a></p>
                                        <p class="question-date">15.04.2016</p>
                                    </div>
                                    <div class="question-rating col-md-6">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </aside>
            </div>
        </div>
    </div>
</div>