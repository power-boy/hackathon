/*--------------------------------------------------------------*/
/*-- JS Document --*/
/*--------------------------------------------------------------*/
(function($) {

    $(document).ready(function() {
            
        $('#back-to-top').hide(); 
        
        setTimeout(function(){ $('.sec-news .news').equalHeights(); }, 1000);
        $('.sec-news .news').equalHeights();        

        
        /*------------------------------------------------------*/
        /*---- Placeholder IE9 -----*/
        /*------------------------------------------------------*/ 
                
        $('input, textarea').placeholder();    
        
        /*------------------------------------------------------*/
        /*---- Select 2 -----*/
        /*------------------------------------------------------*/
         
		$('select').select2();
        
        /*------------------------------------------------------*/
        /*---- iCheck -----*/
        /*------------------------------------------------------*/
         
		$('input[type="radio"], input[type="checkbox"]').iCheck();
                        
        /*------------------------------------------------------*/
        /*---- Window Resize -----*/
        /*------------------------------------------------------*/
        
        $( window ).resize(function() {
         
            $('.sec-news .news').css('height','auto');
            $('.sec-news .news').equalHeights();
            
            banner_margin();
            main_news_height();                        

        });
        
        /*------------------------------------------------------*/
        /*---- Window Scroll -----*/
        /*------------------------------------------------------*/
        
        $(window).scroll(function(){
            if ($(this).scrollTop() > 1) {
                $('#header').addClass('change-color-header');
                $('#back-to-top').fadeIn();
            } else {
                $('#header').removeClass('change-color-header');
                $('#back-to-top').fadeOut();
            }  
        });
        
        /*------------------------------------------------------*/
        /*---- Back to Top -----*/
        /*------------------------------------------------------*/ 
                
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('body,html').animate({
                    scrollTop: 0
            }, 1000);
            return false;
        });
        
        banner_margin(); 
        main_news_height();
        
    });
    
    $.fn.equalHeights = function() {
        var maxHeight = 0,
        $this = $(this);
    
        $this.each( function() {
            var height = $(this).innerHeight();
    
            if ( height > maxHeight ) { maxHeight = height; }
        });
    
        return $this.css('height', maxHeight);
    };
    // auto-initialize plugin
    $('[data-equal]').each(function(){
        var $this = $(this),
        target = $this.data('equal');
        $this.find(target).equalHeights();
    });
     
})(jQuery);
    
    