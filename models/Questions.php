<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Questions".
 *
 * @property integer $id
 * @property string $title
 */
class Questions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questions';
    }
}
