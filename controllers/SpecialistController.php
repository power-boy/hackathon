<?php

namespace app\controllers;

use app\models\Specialist;
use Yii;
use yii\web\Controller;

class SpecialistController extends Controller
{

    public function actionView()
    {
        return $this->render('view');
    }
}
