<?php

namespace app\controllers;

use app\models\Questions;
use Yii;
use yii\web\Controller;

class QuestionController extends Controller
{

    public function actionAdd()
    {
        if ($data = Yii::$app->request->post()) {
            $model = new Questions();
            $model->title = $data['title'];
            $model->category_id = $data['category_id'];
            $model->description = $data['description'];
            if ($data['category_id'] == 1) $model->style_title = 'house';
            if ($data['category_id'] == 2) $model->style_title = 'cars';
            if ($data['category_id'] == 3) $model->style_title = 'recreation';
            if ($data['category_id'] == 4) $model->style_title = 'pets';
            if ($data['category_id'] == 5) $model->style_title = 'sport';
            if ($data['category_id'] == 6) $model->style_title = 'medicine';
            $model->save();
            \Yii::$app->getSession()->setFlash('success', 'Changes have been saved');
            return $this->redirect('/question/popular');
        }
        return $this->render('add');
    }

    public function actionView($id)
    {
        $question = Questions::findOne(array('id' => $id));
        return $this->render('view', array(
            'question' => $question
        ));
    }

    public function actionPopular()
    {
        $questions = Questions::find()
            ->where([])
            ->orderBy('id DESC')
            ->all();
        return $this->render('popular', array(
            'questions' => $questions
        ));
    }

    public function actionSearch($title)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("SELECT * FROM questions WHERE `title` LIKE '%$title%'");
        $questions = $command->queryAll();
        return $this->render('search', array(
            'title' => $title,
            'questions' => $questions
        ));
    }
}
