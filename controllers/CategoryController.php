<?php

namespace app\controllers;

use app\models\Categories;
use app\models\Questions;
use Yii;
use yii\web\Controller;


class CategoryController extends Controller
{

    /**
 * @return string
 */
    public function actionIndex()
    {
        $categories = Categories::find()->all();
        return $this->render('index', [
            'categories' => $categories
        ]);
    }


    public function actionView()
    {
        $id = Yii::$app->request->get('id');
        $category = Categories::findOne($id);
        if (!$category) {
            throw new \yii\web\NotFoundHttpException();
            return false;
        }

        $categoryQuestions = Questions::findAll(['category_id' => $category->id]);
        return $this->render('view', [
            'category' => $category,
            'categoryQuestions' => $categoryQuestions
        ]);
    }
}
